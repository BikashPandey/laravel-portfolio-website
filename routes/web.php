<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Homepage
Route::get('/', 'HomepageController@index')->name('home.page');


//admin Routes
Route::get('/admin', 'backend\AdminController@index')->name('admin.dashboard')->middleware('checkAuth');

//login
Route::get('/login', 'authentication\LoginController@index')->name('admin.show.login');
Route::post('/admin-login', 'authentication\LoginController@login')->name('admin.login');


//logout
Route::get('/logout', 'authentication\LoginController@logout')->name('admin.logout');
