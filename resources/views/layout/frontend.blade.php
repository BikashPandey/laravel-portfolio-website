<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <!--bootstrap 4-->
    <link rel="stylesheet" href="{{ asset('asset/lib/bootstrap/css/bootstrap.min.css')}}">
    <!--fontawesome 5-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!--owl carousel -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
   <!--external css-->
   <link rel="stylesheet" href="{{asset('asset/css/style.css')}}">
   <!--mobile css-->
   <link rel="stylesheet" href="{{asset('asset/css/mobile.css')}}">

  </head>
  <body>
    @yield('content')
  </body>
</html>
<!--jquery-->
<script src="{{asset('asset/lib/jquery/jquery.js')}}" charset="utf-8"></script>
<!--bootstrap 4 js-->
<script src="{{asset('asset/lib/bootstrap/js/bootstrap.min.js')}}" charset="utf-8"></script>
<!--type writter-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/TypewriterJS/1.0.0/typewriter.js" charset="utf-8"></script>
<!--owl carousel -->
<!--scroll reveal-->
<script src="{{asset('asset/lib/scrollreveal/scrollreveal.min.js')}}" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" charset="utf-8"></script>
<!--Main js-->
<script src="{{asset('asset/js/hamburger.js')}}" charset="utf-8"></script>
<script src="{{asset('asset/js/typingjs.js')}}" charset="utf-8"></script>
<script src="{{asset('asset/js/main.js')}}" charset="utf-8"></script>

<script type="text/javascript">
$('a[href*="#"]:not([href="#"]):not([href="#show"]):not([href="#hide"])').click(function() {
  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    if (target.length) {
      $('html,body').animate({
        scrollTop: target.offset().top
      }, 1000);
      return false;
    }
  }
});
</script>
