<form class="form-horizontal" action="{{route('admin.login')}}" method="post">
  @if(Session::has('status'))
    {{Session::get('status')}}
  @endif
  <div class="form-group">
    <input type="email" name="userEmail" class="form-control" placeholder="Enter Email">
  </div>
  <div class="form-group">
    <input type="password" name="userPassword" class="form-control" placeholder="Enter Password">
  </div>
  @csrf
  <div class="form-group">
    <input type="submit" value="Login">
  </div>

</form>
