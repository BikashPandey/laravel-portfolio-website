<?php

namespace App\Http\Controllers\authentication;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;


class LoginController extends Controller
{
    public function index()
    {

      return view('auth.login');
    }

    public function login(Request $request)
    {
      if (Auth::attempt([
        'email' => $request->get('userEmail'),
        'password' => $request->get('userPassword')
      ]))
      {
        return redirect()->route('admin.dashboard');
      }
      else
      {
        return back()->with([
          'status' => 'Invalid Email/Password. Please Try Again'
        ]);
      }

    }

    public function logout()
    {
      Auth::logout();
      return redirect()->route('admin.show.login');
    }
}
