/*skills owl carousel*/
var owl = $('.owl-carousel');
owl.owlCarousel({
    items:4,
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true
});




/* PlaceHolder in text Area (contact form)*/
var textArea = "Hello! \nThis is Bikash Pandey. I've reviewed your CV and have a some work for you. \n\nRegrads \nBikash Pandey"
$('#msg').attr('placeholder', textArea);


/*Work box hover*/
var workBox = $('#work-boxa');
workBox.hover(function(){
  $('#work-box-content').removeClass('d-none');
}, function(){
  $('#work-box-content').addClass('d-none');
});





var goToTopBtn = $('#gototopBtn');

$(window).scroll(function(){

  //currentTop
  var currentTop = $(this).scrollTop();
  //ifuser scrolls
  if (currentTop > 100) {
    goToTopBtn.css('transition', '1s ease');
    goToTopBtn.css('opacity', '1');
  }
  else{
    goToTopBtn.css('transition', '1s ease');
    goToTopBtn.css('opacity', '0');
  }
});

goToTopBtn.click(function(e){
  e.preventDefault();

  $('html, body').animate({
    scrollTop:0
  }, 800);
  return false;
});


// on scroll animation (scrollreveal)
window.sr = ScrollReveal();

sr.reveal('#banner-role',{
  duration: 2000,
  origin: 'top',
  distance: '200px',
});
sr.reveal('.banner-social',{
  duration: 2000,
  origin: 'bottom',
  distance: '200px',
});
sr.reveal('.about-left',{
  duration: 2000,
  origin: 'top',
  distance: '20px',

});
sr.reveal('.about-right',{
  duration: 2000,
  origin: 'bottom',
  distance: '20px',


});
sr.reveal('.skill-',{
  duration: 2000,
  origin: 'bottom',
  distance: '200px',
});
sr.reveal('.work',{
  duration: 1000,
  origin: 'top',
  distance: '100px',
});
sr.reveal('.blog',{
  duration: 1000,
  origin: 'bottom',
  distance: '200px',
});
sr.reveal('.contact',{
  duration: 1000,
  origin: 'left',
  distance: '200px',
});
sr.reveal('.footer',{
  duration: 2000,
  origin: 'bottom',
  distance: '200px',
});
