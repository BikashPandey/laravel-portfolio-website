/*HamBurger toogle*/
document.querySelector('.hamburger').addEventListener('click', (e) => {
	    e.currentTarget.classList.toggle('is-active');

      $('#main').toggleClass('showMenu-support2');
      $('#main').toggleClass('opc');
      $('#menu').toggleClass('showMenu');
			
      $('.menu-icon').toggleClass('showMenu-support1');

      $('.nav-link').click(function(event){
        $('#main').removeClass('showMenu-support2');
        $('#main').removeClass('opc');
        $('#menu').removeClass('showMenu');
        $('.menu-icon').removeClass('showMenu-support1');
        $('.hamburger').removeClass('is-active');

      });

    });
